<?php

namespace Drupal\athenapdf_api\Controller;

use Drupal\athenapdf_api\AthenaPdfConverterInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AthenaPdfController.
 */
class AthenaPdfController extends ControllerBase {

  /**
   * The converter service.
   *
   * @var \Drupal\athenapdf_api\AthenaPdfConverterInterface
   */
  protected $converter;

  /**
   * Constructs a new AthenaPdfController object.
   */
  public function __construct(AthenaPdfConverterInterface $converter) {
    $this->converter = $converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('athenapdf_api.converter')
    );
  }

  /**
   * Print the source markup for the pdf.
   *
   * @param string $id
   *   Id of the item to print.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The markup linked to the given id.
   */
  public function markup($id) {
    $response = new Response();

    $markup = $this->converter->burnAfterReading($id);

    if ($markup) {
      $response->setContent($markup);
      $response->headers->add(['content-type' => 'text/html']);
    }
    else {
      $response->setStatusCode(404);
    }

    return $response;
  }

}
