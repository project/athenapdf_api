<?php

namespace Drupal\athenapdf_api;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class AthenaPdfApiConverter.
 */
class AthenaPdfConverter implements AthenaPdfConverterInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * The http client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The http kernel service.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Url to the endpoint.
   *
   * @var string
   */
  protected $microserviceBaseUrl;

  /**
   * Auth string of the endpoint.
   *
   * @var string
   */
  protected $microserviceAuthString;

  /**
   * Constructs a new AthenaPdfApiConverter object.
   */
  public function __construct(
    StateInterface $state,
    UuidInterface $uuidGenerator,
    Client $httpClient,
    FileSystemInterface $file_system,
    ConfigFactory $configFactory,
    HttpKernelInterface $httpKernel,
    AccountSwitcherInterface $accountSwitcher
  ) {
    $this->state = $state;
    $this->uuidGenerator = $uuidGenerator;
    $this->httpClient = $httpClient;
    $this->fileSystem = $file_system;
    $this->httpKernel = $httpKernel;
    $this->accountSwitcher = $accountSwitcher;

    $config = $configFactory->get('athenapdf_api.settings');
    $this->setEndpoint($config->get('endpoint'), $config->get('auth_key'));
  }

  /**
   * {@inheritDoc}.
   */
  public function addItem(String $markup) {
    $id = $this->uuidGenerator->generate();
    $this->state->set($this->getKey($id), $markup);
    return $id;
  }

  /**
   * {@inheritDoc}.
   */
  public function burnAfterReading(String $id) {
    $key = $this->getKey($id);
    $markup = $this->state->get($key);
    if ($markup) {
      $this->state->delete($key);
    }
    return $markup;
  }

  /**
   * {@inheritDoc}.
   */
  public function urlToPdf($url) {
    $targetUrl = Url::fromUri($this->microserviceBaseUrl, [
      'query' => [
        'auth' => $this->microserviceAuthString,
        'url' => $url,
      ],
    ])->toString();

    $tempName = $this->fileSystem->realpath(
      $this->fileSystem->tempnam('temporary://', 'athenapdf'));

    $this->httpClient->request('GET', $targetUrl, [
      'sink' => $tempName,
    ]);

    return $tempName;
  }

  /**
   * {@inheritDoc}.
   */
  public function markupToPdf($markup) {
    $id = $this->addItem($markup);
    $markupUrl = Url::fromUri("internal:/athenapdf_api/markup/$id", [
      'absolute' => TRUE,
    ])->toString();

    return $this->urlToPdf($markupUrl);
  }

  /**
   * {@inheritDoc}.
   */
  public function requestToPdf(Request $request, AccountInterface $account = NULL) {
    $markup = $this->getMarkupForRequest($request, $account);
    return $this->markupToPdf($markup);
  }

  /**
   * {@inheritDoc}.
   */
  public function internalPathToPdf(String $path, AccountInterface $account = NULL) {
    $request = Request::create($path, 'GET');
    return $this->requestToPdf($request, $account);
  }

  /**
   * {@inheritDoc}.
   */
  public function setEndpoint($url, $authString) {
    $this->microserviceBaseUrl = $url;
    $this->microserviceAuthString = $authString;
  }

  /**
   * {@inheritDoc}.
   */
  public function getMarkupForRequest(Request $request, AccountInterface $account = NULL) {
    if ($account) {
      $this->accountSwitcher->switchTo($account);
    }

    try {
      $response = $this->httpKernel->handle($request, HttpKernelInterface::SUB_REQUEST);
    } finally {
      // Regardless of what happens during the subrequest execution switch the
      // account back.
      if ($account) {
        $this->accountSwitcher->switchBack();
      }
    }

    return $response->getContent();
  }

  /**
   * Returns the state key of the item with given id.
   *
   * @param string $id
   *   Id of the item.
   *
   * @return string
   *   The state key associated with the given id.
   */
  protected function getKey(String $id) {
    return "athenapdf_markup_$id";
  }

}
