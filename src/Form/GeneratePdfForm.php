<?php

namespace Drupal\athenapdf_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\athenapdf_api\AthenaPdfConverterInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GeneratePdfForm.
 */
class GeneratePdfForm extends FormBase {

  /**
   * Drupal\athenapdf_api\AthenaPdfConverterInterface definition.
   *
   * @var \Drupal\athenapdf_api\AthenaPdfConverterInterface
   */
  protected $athenapdfApiConverter;

  /**
   * Constructs a new GeneratePdfForm object.
   */
  public function __construct(
    AthenaPdfConverterInterface $athenapdf_api_converter
  ) {
    $this->athenapdfApiConverter = $athenapdf_api_converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('athenapdf_api.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'athenapdf_api_generate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Uri'),
      '#description' => $this->t('The uri of the content to generate. It can be an absolute URL e.g. <em>https://drupal.org</em> or an internal path like <em>internal:/node/1</em>.'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $outputFile = $this->athenapdfApiConverter
      ->urlToPdf($form_state->getValue('uri'));

    $content = file_get_contents($outputFile);

    $response = new Response();
    // Set headers.
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Content-type', 'application/pdf');
    $response->headers->set('Content-Disposition', 'attachment; filename="file.pdf"');
    $response->headers->set('Content-Transfer-Encoding', 'binary');
    $response->headers->set('Cache-control', 'private');
    $response->headers->set('Content-length', strlen($content));

    $response->setContent($content);

    $form_state->setResponse($response);
  }

}
