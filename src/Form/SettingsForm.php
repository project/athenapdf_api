<?php

namespace Drupal\athenapdf_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'athenapdf_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('athenapdf_api.settings');
    $form['endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Convert endpoint'),
      '#description' => $this->t('The URL of the convert endpoint, e.g. http://dockerhost:8080/convert.'),
      '#default_value' => $config->get('endpoint'),
    ];
    $form['auth_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth key'),
      '#description' => $this->t('The endpoint\'s auth key.'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('auth_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('athenapdf_api.settings')
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->set('auth_key', $form_state->getValue('auth_key'))
      ->save();
  }

}
