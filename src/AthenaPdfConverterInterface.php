<?php

namespace Drupal\athenapdf_api;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface AthenaPdfApiConverterInterface.
 */
interface AthenaPdfConverterInterface {

  /**
   * Generates a pdf from a given url.
   *
   * @param string $url
   *   An absolute URL to generate the PDF from.
   *
   * @return string
   *   Path to the output file.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function urlToPdf($url);

  /**
   * Returns a pdf generated from given markup.
   *
   * @param string $markup
   *   The HTML markup to generate the DPF from.
   *
   * @return string
   *   Path to the output file.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function markupToPdf($markup);

  /**
   * Evaluates the given request and turns its content into a PDF.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The internal request to evaluate.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account to evaluate the given request as.
   *
   * @return false|string
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function requestToPdf(Request $request, AccountInterface $account = NULL);

  /**
   * Evaluates the given internal path and turns its content into a PDF.
   *
   * @param string $path
   *   An internal path.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account to evaluate the given path as.
   *
   * @return false|string
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function internalPathToPdf(String $path, AccountInterface $account = NULL);

  /**
   * Returns the markup stored under the given id.
   *
   * @param string $markup
   *   The markup to save under the given id.
   *
   * @return string
   *   The id under which the item is available..
   */
  public function addItem(String $markup);

  /**
   * Returns the markup stored under the given id and removes it.
   *
   * @param string $id
   *   The unique id.
   *
   * @return string|null
   *   The markup or NULL.
   */
  public function burnAfterReading(String $id);

  /**
   * Sets the endpoint data.
   *
   * @param string $url
   *   The url of the endpoint.
   * @param string $authString
   *   The auth string.
   */
  public function setEndpoint($url, $authString);

  /**
   * Return the HTML for the given request and user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account.
   *
   * @return false|string
   *
   * @throws \Exception
   */
  public function getMarkupForRequest(Request $request, AccountInterface $account = NULL);

}
