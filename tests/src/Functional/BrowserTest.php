<?php

namespace Drupal\Tests\athenapdf_api\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group athenapdf_api
 */
class BrowserTest extends BrowserTestBase {

  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['athenapdf_api'];

  /**
   * The converter service.
   *
   * @var \Drupal\athenapdf_api\AthenaPdfConverterInterface
   */
  protected $converter;

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * TestThe test markup.
   *
   * @var string
   */
  protected $markup = '<html lang="en"><body>Testing</body></html>';

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->converter = $this->container->get('athenapdf_api.converter');
    $this->finfo = finfo_open(FILEINFO_MIME_TYPE);
    $this->user = $this->drupalCreateUser(['access content']);

    if (getenv('ATHENAPDF_ENDPOINT')) {
      $this->converter->setEndpoint(
        getenv('ATHENAPDF_ENDPOINT'),
        getenv('ATHENAPDF_AUTH_STRING')
      );
    }
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalLogin($this->user);
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests that the markup controller works as expected.
   */
  public function testMarkupController() {
    $id = $this->converter->addItem($this->markup);
    $uri = "internal:/athenapdf_api/markup/$id";

    $this->drupalGet(Url::fromUri($uri));
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->responseContains($this->markup);

    // The item should be burnt after reading.
    $this->drupalGet(Url::fromUri($uri));
    $session = $this->assertSession();
    $session->statusCodeEquals(404);
  }

  // TODO: Move the code below to a kernel test.

  // TODO: Add a testing docker-compose file with the Athena service.

  public function testMarkupToPdf() {
    try {
      $this->converter->markupToPdf($this->markup);
    } catch (ServerException $e) {
      // Server exceptions are fine.
    }
  }

  public function testExternalUrl() {
    $pdf = $this->converter->urlToPdf('https://drupal.org');
    $this->assertTrue(finfo_file($this->finfo, $pdf) === 'application/pdf');
  }

  public function testInternalUrl() {
    $request = Request::create('/', 'GET');
    $content = $this->converter->getMarkupForRequest($request);
    $this->assertTrue(strpos($content,
        'Enter your Drupal username') !== FALSE,
      'The content for the anonymous in user is correct.');

    $request = Request::create('/user/2', 'GET');
    $content = $this->converter->getMarkupForRequest($request, $this->user);
    $this->assertTrue(strpos($content,
        'Member for') !== FALSE,
      'The content for the logged in user is correct.');
  }

}
